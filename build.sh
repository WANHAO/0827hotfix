#!/bin/sh

KEY="4345756f4765505a5a7257676a6b6659"
IV="514f7675504c7179534468566a616a43"

WORK_DIR=`pwd`
echo "packaging at work dir: ${WORK_DIR}"
echo "start packaging..."
if [ -d "patch" ]; then 
	mv patch patch_bak
fi
mkdir patch
cp `ls | find . -name "*.lua"` patch/
zip patch_tmp.zip patch/*
rm -rf patch/
echo "finish packaging..."
echo "start encrypting"
openssl enc -aes-128-cbc -in patch_tmp.zip -out patch_enc.zip -K $KEY -iv $IV
rm patch_tmp.zip
echo "finish encrypting"
echo "start packaging again"
zip patch.zip patch_enc.zip
rm patch_enc.zip
echo "end of packaging again"
echo `md5 patch.zip`
